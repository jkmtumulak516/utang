package uhack.utang.main.hubfinder;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uhack.utang.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HubFinderFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HubFinderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HubFinderFragment extends Fragment {

    public HubFinderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     *
     */
    // TODO: Rename and change types and number of parameters
    public static HubFinderFragment newInstance() {
        HubFinderFragment fragment = new HubFinderFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_hub_finder, container, false);
    }


}
