package uhack.utang.main.receive;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import uhack.utang.R;
import uhack.utang.model.LoanModel;

public class ReceiveActivity extends AppCompatActivity {

    private ReceivePresenter receivePresenter;

    private ListView receiveList;
    private ReceiveListAdapter receiveListAdapter;

    public Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive);

        receivePresenter = new ReceivePresenter(this);

        receiveListAdapter= new ReceiveListAdapter(getLayoutInflater(), new ArrayList<LoanModel>());

        receiveList = (ListView) findViewById(R.id.loanList);
        receiveList.setAdapter(receiveListAdapter);

        receivePresenter.loadList();

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.receive_modal);



        receiveList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final LoanModel loanModel = (LoanModel) receiveList.getItemAtPosition(i);
                ((TextView) dialog.findViewById(R.id.txtName)).setText(loanModel.borrowerName);
                ((TextView) dialog.findViewById(R.id.txtAmountToPay)).setText(loanModel.initialAmount + "");
                ((Button) dialog.findViewById(R.id.btnReceivePayment)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        receivePresenter.pay(loanModel.id,Double.parseDouble(((TextView) dialog.findViewById(R.id.txtAmount)).getText().toString()));
                    }
                });
                dialog.show();
            }
        });
    }

    public void closeModal(){
        dialog.dismiss();
    }

    public void updateList(List<LoanModel> loans) {

        receiveListAdapter.setList(loans);

        receiveListAdapter.notifyDataSetChanged();
    }
}
