package uhack.utang.main.receive;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Map;

import uhack.utang.model.LoanModel;

/**
 * Author: King
 * Date: 7/16/2017
 */

public class ReceivePresenter {

    private final ReceiveActivity receiveActivity;
    private final ReceiveData receiveData;

    public ReceivePresenter(ReceiveActivity receiveActivity) {

        this.receiveActivity = receiveActivity;

        receiveData = new ReceiveData();
    }

    public void loadList() {

        receiveData.loadList(new LoadListListener(receiveActivity));
    }

    public void pay(String loadId, double amount){
        receiveData.payLoan(loadId,amount);
        receiveActivity.closeModal();
    }

    private class LoadListListener implements ValueEventListener {

        private ReceiveActivity receiveActivity;

        public LoadListListener(ReceiveActivity receiveActivity) {

            this.receiveActivity = receiveActivity;
        }

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            if (dataSnapshot != null) {

                GenericTypeIndicator<Map<String, LoanModel>> type = new GenericTypeIndicator<Map<String, LoanModel>>() {};

                Map<String, LoanModel> loans = dataSnapshot.getValue(type);

                if(loans != null)
                    receiveActivity.updateList(new  ArrayList<>(loans.values()));
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    }
}
