package uhack.utang.main.receive;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

/**
 * Author: King
 * Date: 7/16/2017
 */

public class ReceiveData {

    private final DatabaseReference databaseReference;

    public ReceiveData() {

        databaseReference = FirebaseDatabase.getInstance().getReference();
    }

    public void loadList(ValueEventListener listener) {

        DatabaseReference loanReference = databaseReference.child("loans");

        loanReference.addListenerForSingleValueEvent(listener);
    }

    public void payLoan(String loanId, double amount) {

        DatabaseReference loanReference = databaseReference.child("loans");

        DatabaseReference transactionReference = loanReference.child(loanId).child("paid").push();


        transactionReference.child("timestamp").setValue(ServerValue.TIMESTAMP);
        transactionReference.child("amount").setValue(amount);
    }
}
