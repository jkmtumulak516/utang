package uhack.utang.main.receive;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import uhack.utang.R;
import uhack.utang.model.BorrowerModel;
import uhack.utang.model.LoanModel;
import uhack.utang.utils.Time;

/**
 * Author: King
 * Date: 7/16/2017
 */

public class ReceiveListAdapter
        extends BaseAdapter {

    private List<LoanModel> loans;

    private LayoutInflater layoutInflater;

    public ReceiveListAdapter(LayoutInflater layoutInflater, List<LoanModel> loans) {

        this.layoutInflater = layoutInflater;

        this.loans = loans;
    }

    @Override
    public int getCount() {
        return loans.size();
    }

    @Override
    public Object getItem(int position) {
        return loans.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // @TODO customize `receive_item` View properly
        View item = layoutInflater.inflate(R.layout.loan_item, parent, false);
        TextView date = (TextView) item.findViewById(R.id.txtTime);
        TextView name = (TextView) item.findViewById(R.id.txtBorrowerName);
        TextView amount = (TextView) item.findViewById(R.id.txtBorrowedMoney);


        LoanModel loan = loans.get(position);

        NumberFormat formatter = new DecimalFormat("#.00");

        date.setText(Time.dateAndTime(loan.date));
        name.setText(loan.borrowerName);
        amount.setText("₱" + formatter.format(loan.initialAmount));

        return item;
    }

    public List<LoanModel> getList() {
        return loans;
    }

    public void setList(List<LoanModel> list) {
        loans = list;
    }
}
