package uhack.utang.main.home;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import uhack.utang.model.HubModel;

/**
 * Author: King
 * Date: 7/15/2017
 */

public class HomePresenter {

    private final HomeFragment homeFragment;
    private final HomeData homeData;

    public HomePresenter(HomeFragment homeFragment) {

        this.homeFragment = homeFragment;

        homeData = new HomeData();
    }

    public void loadHome(String id) {

        if (id != null) {
            homeData.loadHome(id, new HomeLoadListener(homeFragment));
        }
    }

    public void loadReceive() {
        homeFragment.loadReceive();
    }

    public void loadDisburse() {
        homeFragment.showDisburseModal();
    }

    public void loadAddNew() {
        homeFragment.loadAddNewBorrower();
    }

    private class HomeLoadListener implements ValueEventListener {

        private final HomeFragment homeFragment;

        public HomeLoadListener(HomeFragment homeFragment) {

            this.homeFragment = homeFragment;
        }

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            if (dataSnapshot != null) {

                HubModel hubModel = dataSnapshot.getValue(HubModel.class);

                homeFragment.updateHome(hubModel);
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    }


}
