package uhack.utang.main.home;

import java.text.NumberFormat;
import java.text.DecimalFormat;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import uhack.utang.R;
import uhack.utang.main.borrowers.firsttime.AddBorrower;
import uhack.utang.main.receive.ReceiveActivity;
import uhack.utang.model.HubModel;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    private static final String HUB_ID = "Hub Id";

    private HomePresenter homePresenter;

    private String hubId;
    private View fragmentView;

    @BindView(R.id.txtAccountName)
    public TextView accountName;

    @BindView(R.id.txtPendingBalance)
    public TextView pendingBalance;

    @BindView(R.id.txtDisbursed)
    public TextView disbursed;

    @BindView(R.id.txtDues)
    public TextView dues;

    public Dialog d;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     *
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String hubId) {
        HomeFragment fragment = new HomeFragment();

        Bundle args = new Bundle();
        args.putString(HUB_ID, hubId);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        homePresenter = new HomePresenter(this);

        if (getArguments() != null) {
            hubId = getArguments().getString(HUB_ID);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_home, container, false);
        homePresenter.loadHome(hubId);
        return fragmentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        d = new Dialog(this.getContext());
        d.setTitle("Disburse");
        d.setContentView(R.layout.disburse_modal);
        Button btnReceive = (Button) fragmentView.findViewById(R.id.btnReceive);
        btnReceive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homePresenter.loadReceive();
            }
        });
        Button btnFirstTime = (Button) d.findViewById(R.id.btnFirstTime);
        Button btnExisting = (Button) d.findViewById(R.id.btnExisting);
        Button btnPayment = (Button) fragmentView.findViewById(R.id.btnPayment);


        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homePresenter.loadDisburse();
            }
        });

        btnFirstTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homePresenter.loadAddNew();
            }
        });

        btnExisting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


    }

    public void loadAddNewBorrower(){
        Intent i = new Intent(fragmentView.getContext(), AddBorrower.class);
        getActivity().startActivity(i);
    }

    public void loadReceive(){
        Intent i = new Intent(fragmentView.getContext(), ReceiveActivity.class);
        getActivity().startActivity(i);
    }

    public void updateHome(HubModel hubModel) {

        NumberFormat formatter = new DecimalFormat("#.00");

        TextView accountName = (TextView) fragmentView.findViewById(R.id.txtAccountName);
        TextView pendingBalance = (TextView) fragmentView.findViewById(R.id.txtPendingBalance);
        TextView disbursed = (TextView) fragmentView.findViewById(R.id.txtDisbursed);
        TextView dues = (TextView) fragmentView.findViewById(R.id.txtDues);

        accountName.setText(hubModel.name);
        pendingBalance.setText("₱" + formatter.format(hubModel.pendingBalance));
        disbursed.setText("₱" + formatter.format(hubModel.disbursed));
        dues.setText("₱" + formatter.format(hubModel.dues));
    }

    public void showDisburseModal(){


        d.show();
    }
}

