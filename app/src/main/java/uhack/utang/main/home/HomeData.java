package uhack.utang.main.home;

import android.provider.ContactsContract;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Author: King
 * Date: 7/15/2017
 */

public class HomeData {

    private DatabaseReference databaseReference;

    public HomeData() {

        databaseReference = FirebaseDatabase.getInstance().getReference();
    }

    public void loadHome(String id, ValueEventListener listener) {

        DatabaseReference hubReference = databaseReference.child("hubs").child(id);

        hubReference.addListenerForSingleValueEvent(listener);
    }

}
