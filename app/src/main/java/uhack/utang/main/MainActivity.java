package uhack.utang.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import uhack.utang.R;
import uhack.utang.main.borrowers.BorrowersFragment;
import uhack.utang.main.home.HomeFragment;
import uhack.utang.main.hubfinder.HubFinderFragment;
import uhack.utang.main.settings.SettingsFragment;
import uhack.utang.main.transactions.TransactionsFragment;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;


    public ViewPager viewPager;


    public BottomNavigationView bottomNavigationView;

    public HomeFragment homeFragment;
    public TransactionsFragment transactionsFragment;
    public BorrowersFragment borrowersFragment;
    public HubFinderFragment hubFinderFragment;
    public SettingsFragment settingsFragment;

    public FirebaseAuth mAuth;

    public MenuItem prevMenuItem;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_transactions:
                    viewPager.setCurrentItem(1);
                    return true;
                case R.id.navigation_borrowers:
                    viewPager.setCurrentItem(2);
                    return true;
                case R.id.navigation_hub_finder:
                    viewPager.setCurrentItem(3);
                    return true;
                case R.id.navigation_settings:
                    viewPager.setCurrentItem(4);
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        //setup view pager
        setupViewPager(viewPager);

        //setup bottom navigation
        setupBottomNavigationWithViewPager();
    }

    private void setupViewPager(ViewPager viewPager)
    {
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        String uid = user.getUid();
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        // @TODO replace with real variable/value
        homeFragment = HomeFragment.newInstance(uid);
        transactionsFragment = TransactionsFragment.newInstance();
        borrowersFragment = BorrowersFragment.newInstance();
        hubFinderFragment = HubFinderFragment.newInstance();
        settingsFragment = SettingsFragment.newInstance();
        adapter.addFragment(homeFragment);
        adapter.addFragment(transactionsFragment);
        adapter.addFragment(borrowersFragment);
        adapter.addFragment(hubFinderFragment);
        adapter.addFragment(settingsFragment);
        viewPager.setAdapter(adapter);
    }

    private void setupBottomNavigationWithViewPager(){
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                }
                else
                {
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                }

                bottomNavigationView.getMenu().getItem(position).setChecked(true);
                prevMenuItem = bottomNavigationView.getMenu().getItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

}
