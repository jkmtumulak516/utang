package uhack.utang.main.dues;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import uhack.utang.R;

public class DuesActivity extends AppCompatActivity {

    private DuesPresenter duesPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dues2);

        duesPresenter = new DuesPresenter(this);

        duesPresenter.update();
    }

    public void update(double amount) {

        NumberFormat formatter = new DecimalFormat("0.00");

        TextView dues = (TextView) findViewById(R.id.dues);

        dues.setText("₱" + formatter.format(amount));
    }
}
