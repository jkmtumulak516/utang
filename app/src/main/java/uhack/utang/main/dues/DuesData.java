package uhack.utang.main.dues;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import android.provider.ContactsContract;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ValueEventListener;

/**
 * Author: King
 * Date: 7/16/2017
 */

public class DuesData {

    private final DatabaseReference databaseReference;

    public DuesData() {

        databaseReference = FirebaseDatabase.getInstance().getReference();
    }


    public void update(ValueEventListener listener) {

        DatabaseReference hubReference = databaseReference.child("hubs").child(FirebaseAuth.getInstance().getCurrentUser().getUid());


        hubReference.addListenerForSingleValueEvent(listener);
    }
}
