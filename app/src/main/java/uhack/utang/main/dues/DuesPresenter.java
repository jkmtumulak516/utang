package uhack.utang.main.dues;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import uhack.utang.model.HubModel;

/**
 * Author: King
 * Date: 7/16/2017
 */

public class DuesPresenter {

    private final DuesActivity duesActivity;
    private final DuesData duesData;

    public DuesPresenter(DuesActivity duesActivity) {

        this.duesActivity = duesActivity;

        duesData = new DuesData();
    }

    public void update() {

        duesData.update(new UpdateLister(duesActivity));
    }

    public void pay() {

    }

    private class UpdateLister implements ValueEventListener {

        private final DuesActivity duesActivity;

        public UpdateLister(DuesActivity duesActivity) {

            this.duesActivity = duesActivity;
        }

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            HubModel hub = dataSnapshot.getValue(HubModel.class);

            if (dataSnapshot != null && hub != null) {

                duesActivity.update(hub.dues);

            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    }
}
