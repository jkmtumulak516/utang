package uhack.utang.main.disburse.search;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Author: King
 * Date: 7/15/2017
 */

public class SearchData {

    private final DatabaseReference databaseReference;

    public SearchData() {

        databaseReference = FirebaseDatabase.getInstance().getReference();
    }

    public void loadList(ValueEventListener listener) {

        DatabaseReference borrowersReference = databaseReference.child("borrowers");

        borrowersReference.addListenerForSingleValueEvent(listener);
    }
}
