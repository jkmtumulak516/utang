package uhack.utang.main.disburse;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import org.w3c.dom.Text;

import java.util.Date;

import uhack.utang.R;

public class DisburseActivity extends AppCompatActivity {
    public String borrowerId;
    public String borrowerName;

    public TextView txtReceiver;
    public EditText txtAmount;
    public EditText txtReason;
    public EditText txtDateToPay;
    public Button btnAddDispurse;

    public DisbursePresenter disbursePresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disburse);

        disbursePresenter = new DisbursePresenter(this);

        txtReceiver = (TextView) findViewById(R.id.txtReceiver);
        txtAmount = (EditText) findViewById(R.id.txtAmount);
        txtReason = (EditText) findViewById(R.id.txtReason);
        txtDateToPay = (EditText) findViewById(R.id.txtDateToPay);
        btnAddDispurse = (Button) findViewById(R.id.btnAddDisburse);

        Bundle bundle = getIntent().getExtras();
        borrowerId = bundle.getString("borrowerId");
        borrowerName = bundle.getString("borrowerName");

        if(borrowerId != null && borrowerName != null){
            this.borrowerId = borrowerId;
            this.borrowerName = borrowerName;
            txtReceiver.setText(borrowerName);
        }

        btnAddDispurse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String hubId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                long date = Date.parse(txtDateToPay.getText().toString());
                disbursePresenter.createLoan(hubId,borrowerId,borrowerName,Double.parseDouble(txtAmount.getText().toString()),date);

            }
        });
    }

    public void finishDisburse(){
        finish();
    }
}
