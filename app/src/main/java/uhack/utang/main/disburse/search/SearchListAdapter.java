package uhack.utang.main.disburse.search;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import uhack.utang.R;
import uhack.utang.model.BorrowerModel;

/**
 * Author: King
 * Date: 7/15/2017
 */

public class SearchListAdapter
        extends BaseAdapter
        implements Filterable {

    private List<BorrowerModel> borrowers;

    private LayoutInflater layoutInflater;

    public SearchListAdapter(LayoutInflater layoutInflater, List<BorrowerModel> borrowers) {

        this.layoutInflater = layoutInflater;

        this.borrowers = borrowers;
    }

    @Override
    public int getCount() {
        return borrowers.size();
    }

    @Override
    public Object getItem(int position) {
        return borrowers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // @TODO customize `search_item` View properly
        View item = layoutInflater.inflate(R.layout.search_item, parent, false);

        BorrowerModel borrower = borrowers.get(position);

        // @TODO set all necessary properties
        TextView nameView = (TextView) item.findViewById(R.id.borrowerName);

        nameView.setText(borrower.name);

        return item;
    }

    public List<BorrowerModel> getList() {
        return borrowers;
    }

    public void setList(List<BorrowerModel> list) {
        borrowers = list;
    }

    @Override
    public Filter getFilter() {
        return new SearchFilter(this, borrowers);
    }

    public static class SearchFilter
            extends Filter {

        private SearchListAdapter searchListAdapter;

        private List<BorrowerModel> list;
        private List<BorrowerModel> filteredList;

        public SearchFilter(SearchListAdapter searchListAdapter, List<BorrowerModel> list) {

            this.searchListAdapter = searchListAdapter;

            this.list = list;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            final FilterResults results = new FilterResults();

            filteredList = new ArrayList<>();

            final String filterPattern = "(?i).*" + constraint.toString().trim() + ".*";

            for (BorrowerModel borrower : list) {

                if (borrower.name.matches(filterPattern)) {

                    filteredList.add(borrower);
                }
            }

            results.count = filteredList.size();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            if (searchListAdapter.getList() != null && results.values != null) {
                searchListAdapter.notifyDataSetChanged();
            }
        }
    }
}
