package uhack.utang.main.disburse.newborrower;

import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.ListView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.List;

import uhack.utang.model.BorrowerModel;

/**
 * Author: King
 * Date: 7/15/2017
 */

public class NewBorrowerData {

    private final DatabaseReference databaseReference;
    private final StorageReference storageReference;

    public NewBorrowerData() {

        databaseReference = FirebaseDatabase.getInstance().getReference();

        storageReference = FirebaseStorage.getInstance().getReference();
    }

    public void createBorrower(BorrowerModel borrowerModel, Uri image) {

        final DatabaseReference borrowerReference = databaseReference.child("borrowers").push();

        borrowerReference.setValue(borrowerModel);

        if (image != null) {

            StorageReference imageReference = storageReference.child("documents");

            imageReference.child(borrowerReference.getKey()).putFile(image) // if each post is limited to 1 image each
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // store image of uploaded image
                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                        borrowerReference.child("document").setValue(downloadUrl.toString());
                    }
                });

        }
    }
}
