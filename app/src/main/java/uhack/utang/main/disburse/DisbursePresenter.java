package uhack.utang.main.disburse;

import java.util.Date;

import uhack.utang.model.LoanModel;

/**
 * Author: King
 * Date: 7/15/2017
 */

public class DisbursePresenter {

    private final DisburseActivity disburseActivity;
    private final DisburseData disburseData;

    public DisbursePresenter(DisburseActivity disburseActivity) {

        this.disburseActivity = disburseActivity;

        disburseData = new DisburseData();
    }

    public void createLoan(String hubId, String borrowerId, String borrowerName, double amount, long date) {


        LoanModel loanModel = new LoanModel();
        loanModel.hubId = hubId;
        loanModel.borrowerId = borrowerId;
        loanModel.borrowerName = borrowerName;
        loanModel.initialAmount = amount;
        loanModel.date = date;
        disburseData.createLoan(loanModel);

        disburseActivity.finishDisburse();
    }

}
