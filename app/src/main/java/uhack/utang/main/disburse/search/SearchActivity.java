package uhack.utang.main.disburse.search;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;


import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import uhack.utang.R;
import uhack.utang.model.BorrowerModel;

public class SearchActivity extends AppCompatActivity {

    private SearchPresenter searchPresenter;

    private ListView searchList;
    private SearchListAdapter searchListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        searchPresenter = new SearchPresenter(this);

        searchListAdapter = new SearchListAdapter(getLayoutInflater(), new ArrayList<BorrowerModel>());

        searchList = (ListView) findViewById(R.id.borrowerList);
        searchList.setAdapter(searchListAdapter);

        searchPresenter.loadList();
    }

    public void updateList(List<BorrowerModel> borrowers) {

        //searchListAdapter = new SearchListAdapter(getLayoutInflater());

        searchListAdapter.setList(borrowers);

        searchListAdapter.notifyDataSetChanged();
    }

}
