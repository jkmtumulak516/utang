package uhack.utang.main.disburse.search;


import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Map;

import uhack.utang.model.BorrowerModel;

/**
 * Author: King
 * Date: 7/15/2017
 */

public class SearchPresenter {

    private final SearchActivity searchActivity;
    private final SearchData searchData;

    public SearchPresenter(SearchActivity searchActivity) {

        this.searchActivity = searchActivity;

        searchData = new SearchData();
    }

    public void loadList() {

        searchData.loadList(new LoadListListener(searchActivity));
    }

    private class LoadListListener implements ValueEventListener {

        private SearchActivity searchActivity;

        public LoadListListener(SearchActivity searchActivity) {

            this.searchActivity = searchActivity;
        }

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            if (dataSnapshot != null) {

                GenericTypeIndicator<Map<String, BorrowerModel>> type = new GenericTypeIndicator<Map<String, BorrowerModel>>() {};

                Map<String, BorrowerModel> borrowers = dataSnapshot.getValue(type);

                searchActivity.updateList(new ArrayList<>(borrowers.values()));
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    }
}
