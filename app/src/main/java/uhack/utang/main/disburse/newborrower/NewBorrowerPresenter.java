package uhack.utang.main.disburse.newborrower;

/**
 * Author: King
 * Date: 7/15/2017
 */

public class NewBorrowerPresenter {

    private final NewBorrowerFragment newBorrowerFragment;
    private final NewBorrowerData newBorrowerData;

    public NewBorrowerPresenter(NewBorrowerFragment newBorrowerFragment) {

        this.newBorrowerFragment = newBorrowerFragment;

        newBorrowerData = new NewBorrowerData();
    }

    public void createBorrower() {

        // @TODO 

        // gather information for borrower

        // create borrowerModel:
        // BorrowerModel borrowerModel;

        // call function:
        // newBorrowerData.createBorrower(borrowerModel);

        // do whatever else after
    }
}
