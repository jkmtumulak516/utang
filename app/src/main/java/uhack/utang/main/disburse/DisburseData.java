package uhack.utang.main.disburse;

import android.provider.ContactsContract;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import uhack.utang.model.LoanModel;
import uhack.utang.model.TransactionModel;

/**
 * Author: King
 * Date: 7/15/2017
 */

public class DisburseData {

    private final DatabaseReference databaseReference;

    public DisburseData() {

        databaseReference = FirebaseDatabase.getInstance().getReference();
    }

    public void createLoan(LoanModel loanModel) {

        final DatabaseReference loanReference = databaseReference.child("loans").push();

        loanModel.id = loanReference.getKey();

        loanReference.setValue(loanModel);
    }
}
