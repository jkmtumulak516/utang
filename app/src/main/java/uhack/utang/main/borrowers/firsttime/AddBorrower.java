package uhack.utang.main.borrowers.firsttime;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import uhack.utang.R;
import uhack.utang.main.disburse.DisburseActivity;

public class AddBorrower extends AppCompatActivity {
    AddBorrowerPresenter addBorrowerPresenter;

    EditText newBorrowerName;
    ImageView imgView;
    ImageView imgAdd;
    Button btnRegisterBorrower;
    Intent data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_borrower);
        addBorrowerPresenter = new AddBorrowerPresenter(this);

        newBorrowerName = (EditText) findViewById(R.id.txtNewBorrowerName);
        imgAdd = (ImageView) findViewById(R.id.imgAdd);
        imgView = (ImageView) findViewById(R.id.imgView);
        btnRegisterBorrower = (Button) findViewById(R.id.btnRegisterBorrower);

        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addBorrowerPresenter.loadCamera();

            }
        });

        btnRegisterBorrower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addBorrowerPresenter.addBorrower(data,newBorrowerName.getText().toString());
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1 && resultCode == RESULT_OK){
            this.data = data;
            addBorrowerPresenter.handleResult(data);
        }
    }

    public void imageView(Bitmap photo){
        imgAdd.setVisibility(View.INVISIBLE);
        imgView.setImageBitmap(photo);
        imgView.setVisibility(View.VISIBLE);
    }

    public void loadDisburseScreen(String borrowerId, String borrowerName){
        Intent i = new Intent(this, DisburseActivity.class);
        i.putExtra("borrowerId",borrowerId);
        i.putExtra("borrowerName",borrowerName);

        startActivity(i);
        finish();

    }
}
