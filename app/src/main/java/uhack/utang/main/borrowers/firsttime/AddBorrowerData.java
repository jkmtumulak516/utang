package uhack.utang.main.borrowers.firsttime;

import android.net.Uri;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;

import uhack.utang.model.BorrowerModel;
import uhack.utang.utils.Time;

/**
 * Author: King
 * Date: 7/16/2017
 */

public class AddBorrowerData {

    private final DatabaseReference databaseReference;
    private final StorageReference storageReference;

    public AddBorrowerData() {

        databaseReference = FirebaseDatabase.getInstance().getReference();

        storageReference = FirebaseStorage.getInstance().getReference();
    }

    public void addBorrower(BorrowerModel borrowerModel, Uri image) {

        final DatabaseReference borrowerReference = databaseReference.child("borrowers").push();

        borrowerModel.id = borrowerReference.getKey();

        borrowerModel.creditRating = 50;

        borrowerReference.setValue(borrowerModel);

        if (image != null) {

            StorageReference imageReference = storageReference.child("documents");

            imageReference.child(borrowerReference.getKey()).putFile(image) // if each post is limited to 1 image each
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            // store image of uploaded image
                            Uri downloadUrl = taskSnapshot.getDownloadUrl();
                            borrowerReference.child("document").setValue(downloadUrl.toString());
                        }
                    });

        }
    }
}
