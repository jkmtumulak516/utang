package uhack.utang.main.borrowers.firsttime;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;

import uhack.utang.model.BorrowerModel;

/**
 * Created by jemarjudemaranga on 16/07/2017.
 */

public class AddBorrowerPresenter {

    private AddBorrower addBorrowerView;
    private AddBorrowerData addBorrowerData;

    public AddBorrowerPresenter(AddBorrower addBorrowerView) {

        this.addBorrowerView = addBorrowerView;
        addBorrowerData = new AddBorrowerData();
    }

    public void loadCamera() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        addBorrowerView.startActivityForResult(cameraIntent, 1);
    }

    public void handleResult(Intent data){
        Bitmap photo = (Bitmap) data.getExtras().get("data");
        addBorrowerView.imageView(photo);

    }

    public void addBorrower(Intent data, String name) {

        // get image
        Uri image = data.getData();
        // get borrower details
        BorrowerModel borrowerModel = new BorrowerModel();
        borrowerModel.name = name;

        addBorrowerData.addBorrower(borrowerModel, image);

        addBorrowerView.loadDisburseScreen(borrowerModel.id,borrowerModel.name);

    }


}
