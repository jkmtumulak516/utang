package uhack.utang.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Author: King
 * Date: 7/15/2017
 */

public class HubModel {

    public String account_no; // union bank id
    public String name;

    public double pendingBalance;
    public double disbursed;
    public double dues;

    public double lat;
    public double lng;

    public String email;

    public LatLng getLatLng() {
        return new LatLng(lat, lng);
    }

}
