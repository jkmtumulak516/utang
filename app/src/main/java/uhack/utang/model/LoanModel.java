package uhack.utang.model;

import java.util.Map;

/**
 * Author: King
 * Date: 7/15/2017
 */

public class LoanModel {
    public String id;
    public String hubId;
    public String borrowerId;
    public String borrowerName;
    public long date; // timestamp
    public double initialAmount;

    // key: date paid, value: amount paid
    public Map<String, TransactionModel> paid;
}
