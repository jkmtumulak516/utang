package uhack.utang.model;

import java.util.Map;

/**
 * Author: King
 * Date: 7/15/2017
 */

public class BorrowerModel {

    public String id; // firebase id
    public String name;

    // key: name of document, value: document url
    public String document;

    // key: date of rating, value: rating
    public double creditRating;

}
