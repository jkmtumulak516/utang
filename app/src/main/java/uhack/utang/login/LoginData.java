package uhack.utang.login;

import android.provider.ContactsContract;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Author: King
 * Date: 7/15/2017
 */

public class LoginData {
    // instance of Firebase or FirebaseAuth or whatever

    // example
    public void callDatabase(ValueEventListener listener) {

        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference();

        // reference some child
        dbRef.child("someChildNode").addValueEventListener(listener);

    }
}
