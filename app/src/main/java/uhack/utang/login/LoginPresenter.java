package uhack.utang.login;

import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import javax.crypto.ExemptionMechanism;

/**
 * Author: King
 * Date: 7/15/2017
 */


public class LoginPresenter {

    private LoginActivity loginActivity;
    private LoginData loginData;
    private FirebaseAuth mAuth;

    LoginPresenter(LoginActivity loginActivity) {
        this.loginActivity = loginActivity;

        loginData = new LoginData();
        mAuth = FirebaseAuth.getInstance();
    }

    // performed when called by activity
    public void login(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(loginActivity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            //Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(loginActivity, "Login Success", Toast.LENGTH_SHORT).show();
                            loginActivity.gotoMain();
                            //updateUI(user);
                        } else {
                            Toast.makeText(loginActivity, "Login Failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        // call data and pass appropriate listener
        loginData.callDatabase(new ExampleListener(loginActivity));
    }

    public class ExampleListener implements ValueEventListener {

        LoginActivity loginActivity;

        public ExampleListener(LoginActivity loginActivity) {

            // stores the activity to be used
            this.loginActivity = loginActivity;
        }

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            // call activity's function or manipulate it manually
            //loginActivity.manipulateView();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    }

}
