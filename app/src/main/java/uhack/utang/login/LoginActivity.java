package uhack.utang.login;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import uhack.utang.R;
import uhack.utang.main.MainActivity;
import uhack.utang.model.LoanModel;
import uhack.utang.utils.Time;

public class LoginActivity extends AppCompatActivity {

    private LoginPresenter loginPresenter;

    public EditText username;

    public EditText password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        loginPresenter = new LoginPresenter(this);

        TEST();
    }

    public void loginClick(View view){
        //gotoMain();
        loginPresenter.login(username.getText().toString(),password.getText().toString());
    }
    // some function to manipulate database
    public void gotoMain() {

        Intent i = new Intent(this,MainActivity.class);
        startActivity(i);
        finish();
    }

    public void TEST() {

        Query query = FirebaseDatabase.getInstance().getReference().child("hubs");

        DatabaseReference ref = query.equalTo(0, "pendingBalance").getRef();

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//        DatabaseReference loanReference = FirebaseDatabase.getInstance().getReference().child("loans").push();
//
//        LoanModel loanModel = new LoanModel();
//        loanModel.initialAmount = 1234;
//        loanModel.date = Time.currentTimestamp();
//        loanModel.hubId = "ChKCmzEZzOd1RJKJuqiWAQr6z6t1";
//
//        loanReference.setValue(loanModel);
    }
}
