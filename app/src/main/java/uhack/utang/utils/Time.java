package uhack.utang.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Author: King
 * Date: 7/16/2017
 */

public class Time {

    public static long currentTimestamp() {

        return System.currentTimeMillis();
    }

    public static String dateAndTime(long timestamp) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(timestamp);

        NumberFormat formatter = new DecimalFormat("00");

        return getMonth(calendar.get(Calendar.MONTH)) + " "
                + calendar.get(Calendar.DATE) + ", "
                + formatter.format(calendar.get(Calendar.HOUR)) + ":"
                + formatter.format(calendar.get(Calendar.MINUTE));
        //+ calendar.get(Calendar.AM_PM);
    }

    private static String getMonth(int month) {
        switch (month) {
            case Calendar.JANUARY:
                return "January";
            case Calendar.FEBRUARY:
                return "February";
            case Calendar.MARCH:
                return "March";
            case Calendar.APRIL:
                return "April";
            case Calendar.MAY:
                return "May";
            case Calendar.JUNE:
                return "June";
            case Calendar.JULY:
                return "July";
            case Calendar.AUGUST:
                return "August";
            case Calendar.SEPTEMBER:
                return "September";
            case Calendar.OCTOBER:
                return "October";
            case Calendar.NOVEMBER:
                return "November";
            case Calendar.DECEMBER:
                return "December";
            default:
                return null;
        }
    }
}
