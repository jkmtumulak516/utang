const functions = require('firebase-functions');

const admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);

exports.createLoanTrigger = 
    functions.database.ref('/loans/{loanId}/initialAmount').onWrite(event => {

        var loanId = event.params.loanId;
        console.log("loanId -> " + loanId);
        var loanRef = event.data.ref.parent;
        console.log("loanRef -> " + loanRef);

        return loanRef.once('value', (snapshot) => {

            var loan = snapshot.val();
            console.log("loan -> " + loan);

            var transaction = {
                'hubId' : loan.hubId,
                'timestamp' : loan.date,
                'amount' : -loan.initialAmount
            };

            admin.database().ref('/transactions/' + loanId).set(transaction);
        });
    });

exports.transactionTrigger =
    functions.database.ref('/transactions/{transactionId}/hubId').onWrite(event => {

        var transactionRef = event.data.ref.parent;

        return transactionRef.once('value', (snapshot) => {
            
            var transaction = snapshot.val();

            var transactionAmount = transaction.amount;
            var transactionHubId = transaction.hubId;

            admin.database().ref('/hubs/' + transactionHubId + '/pendingBalance').once('value', (snapshot) => {

                var currentPendingBalance = snapshot.val();

                snapshot.ref.set(currentPendingBalance + transactionAmount);
            });

            if (transactionAmount < 0) {

                admin.database().ref('/hubs/' + transactionHubId + '/disbursed').once('value', (snapshot) => {

                    var currentDisbursed = snapshot.val();

                    snapshot.ref.set(currentDisbursed - transactionAmount);
                }); 
            }
        });
    }); 